# Explanations for Text Categorization

This repository is part of a bachelor thesis.

```
.
├── README.md           // This README file
├── classifiers         // Implementations for explainable Text Classifiers
├── datasets            // The modified version of the Toxic Comment dataset, that we used in the thesis
├── environment.yml     // A conda environment file to load all dependencies for this repository
├── jupyter_notebooks   // Includes the jupyter notebooks to train the classifiers and evaluate explanations with EPI and word deletion
├── trained_models      // Includes all trained models as they were used in the thesis evaluations
├── webapp              // Includes a implementation of a simple web app to visualize explanations in the browser 
└── wordvectors         // Includes the GloVe word vectors we used in the thesis
```

## Visualizing Explanations

### 20 Newsgroups

To visualize explanations for 20 newsgroups categorizations in the browser navigate into the `webapp` directory and start the server with `python3 server.py ng`.
Once the server loaded all the models and started open `localhost:8091`.

### Toxic Comments

To visualize explanations for toxic comments categorizations in the browser navigate into the `webapp` directory and start the server with `python3 server.py tc`.
Once the server loaded all the models and started open `localhost:8090`.
